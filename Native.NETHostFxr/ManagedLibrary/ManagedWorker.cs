﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace ManagedLibrary
{
    public class ManagedWorker
    {
        [UnmanagedCallersOnly]
        public static void Print()
        {
            Console.WriteLine("ManagedWorker.Print()");
        }

        [UnmanagedCallersOnly]
        public static double Power(double baseValue, double exponent)
        {
            return Math.Pow(baseValue, exponent);
        }
    }
}
