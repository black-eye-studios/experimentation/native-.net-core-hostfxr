#include "DotNetRuntime.h"

#include <iostream>
#include <cassert>
#include <Windows.h>
#include <shlwapi.h>
#include <nethost.h>

namespace Pls
{
    /*---------------------------------------------------------------------------------*/
    /* Constructors/Destructor                                                         */
    /*---------------------------------------------------------------------------------*/
    DotNetRuntime::DotNetRuntime(int argc, const wchar_t** argv)
    {
        // Get the current executable directory
        std::string runtimePath(MAX_PATH, '\0');
        GetModuleFileNameA(nullptr, runtimePath.data(), MAX_PATH);
        PathRemoveFileSpecA(runtimePath.data()); 
        // Since PathRemoveFileSpecA() removes from data(), the size is not updated, so we must manually update it
        runtimePath.resize(std::strlen(runtimePath.data())); 
        
        // Construct the HostFxr path
        char_t hostFxrPath[MAX_PATH];
        size_t buffer_size = sizeof(hostFxrPath) / sizeof(char_t);
        int result = get_hostfxr_path(hostFxrPath, &buffer_size, nullptr);
        if (result != 0)
        {
            throw SystemInitException("[DotNetRuntime] Failed to get HostFxr path!");
        }

        // Load HostFxr
        hostFxrMod = LoadLibraryW(hostFxrPath);
        if (!hostFxrMod)
        {
            throw SystemInitException("[DotNetRuntime] Failed to load HostFxr!");
        }

        // Get HostFxr Functions
        hostFxrInit = getHostFxrFunctionPtr<hostfxr_initialize_for_dotnet_command_line_fn>("hostfxr_initialize_for_dotnet_command_line");
        hostFxrGetDelegate = getHostFxrFunctionPtr<hostfxr_get_runtime_delegate_fn>("hostfxr_get_runtime_delegate");
        hostFxrExit = getHostFxrFunctionPtr<hostfxr_close_fn>("hostfxr_close");

        // Initialize and start the .NET Core Runtime
        result = hostFxrInit(argc, argv, nullptr, &hostFxr);
        if (result != 0 || !hostFxr)
        {
            std::ostringstream oss;
            oss << std::hex << std::showbase 
                << "[DotNetRuntime] Error #" << result << " Failed to initialize HostFxr\n";
            throw SystemInitException(oss.str());
        }

        // Get Function Pointer to delegate
        getManagedFunctionPtr = getCoreClrFunctionPtr<get_function_pointer_fn>(hdt_get_function_pointer);
    }

    DotNetRuntime::~DotNetRuntime()
    {
        // NOTE: Destructors are noexcept, so we output to cerr instead.
        
        // Shutdown the .NET Core runtime
        int result = hostFxrExit(hostFxr);
        if (result != 0)
        {
            std::cerr << std::hex << std::showbase 
                      << "[DotNetRuntime] Failed to shut down .NET Core runtime. Error 0x" << result << "\n";
        }

        // Unload the DLL
        if (!FreeLibrary(hostFxrMod))
        {
            std::cerr  << "[DotNetRuntime] Failed to free \"hostFxr.dll\".\n";
        }
    }
}