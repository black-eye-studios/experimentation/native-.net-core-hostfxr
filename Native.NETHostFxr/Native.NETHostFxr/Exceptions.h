#pragma once

#include <stdexcept>

namespace Pls
{
    /********************************************************************************//*!
     @brief    Exception that should be thrown when failing to initialize a system.
    *//*********************************************************************************/
    class SystemInitException : public std::runtime_error
    {
        // Use constructors from the base class
        using runtime_error::runtime_error;
    };
    /********************************************************************************//*!
     @brief    Exception that should be thrown when failing to shut down a system.
    *//*********************************************************************************/
    class SystemExitException : public std::runtime_error
    {
        // Use constructors from the base class
        using runtime_error::runtime_error;
    };
    /********************************************************************************//*!
     @brief    Exception that should be thrown when failing to retrieve a function
               pointer.
    *//*********************************************************************************/
    class FunctionPtrNotFoundException : public std::runtime_error
    {
        // Use constructors from the base class
        using runtime_error::runtime_error;
    };
}