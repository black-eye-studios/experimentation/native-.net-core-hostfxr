#pragma once

#include <iomanip>             // std::setfill, std::setw
#include <string>              // std::string
#include <sstream>             // std::ostringstream
#include <algorithm>
#include <Windows.h>           // HMODULE
#include <hostfxr.h>           // hostfxr_*
#include <coreclr_delegates.h> // UNMANAGEDCALLERSONLY_METHOD

#include "Exceptions.h"        // FunctionPtrNotFoundException

namespace Pls
{
    /********************************************************************************//*!
    @brief    Class that encapsulates the state of the NET Core Runtime lifecycle.
    *//*********************************************************************************/
    class DotNetRuntime
    {
      public:
        /*-----------------------------------------------------------------------------*/
        /* Type Defintions                                                             */
        /*-----------------------------------------------------------------------------*/
        using string_t = std::basic_string<char_t>;

        /*-----------------------------------------------------------------------------*/
        /* Constructors/Destructor                                                     */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Initializes the DotNetRuntime. Loads the HostFxr and grabs pointers to
                  bootstrapping functions and kickstarts the HostFxr.
        *//*****************************************************************************/
        DotNetRuntime(int argc, const wchar_t** argv);
        /****************************************************************************//*!
        @brief    Unloads the HostFxr.
        *//*****************************************************************************/
        ~DotNetRuntime();

        // Disallow copy and moving
        DotNetRuntime(const DotNetRuntime&) = delete;
        DotNetRuntime(DotNetRuntime&&) = delete;

        /*-----------------------------------------------------------------------------*/
        /* Usage Functions                                                             */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a function pointer from the a CLR assembly based on the 
                  specified assembly, type and function names.

        @tparam        FunctionType
                Type of the function pointer that the specified function name will 
                provide.
                
        @params[in]    assemblyName
                Name of the CoreCLR assembly that contains the function.
        @params[in]    typeName
                Name of the CoreCLR type in the assembly that contains the function.
                Nested types are separated by a period(.).
        @params[in]    functionName
                Name of the CoreCLR function to get a pointer to.

        @returns  Pointer to the function in the assembly that was specified.
        *//*****************************************************************************/
        template<typename FunctionType>
        FunctionType GetFunctionPtr(const string_t& assemblyName, 
                                    const string_t& typeName, 
                                    const string_t& functionName);

      private:
        /*-----------------------------------------------------------------------------*/
        /* Data Members                                                                */
        /*-----------------------------------------------------------------------------*/
        // References to HostFxr key components
        HMODULE hostFxrMod;
        hostfxr_handle hostFxr;

        // Function Pointers to HostFxr functions
        hostfxr_initialize_for_dotnet_command_line_fn hostFxrInit;
        hostfxr_get_runtime_delegate_fn               hostFxrGetDelegate;
        hostfxr_close_fn                              hostFxrExit;

        // Function Pointers to CoreClr functions
        get_function_pointer_fn                       getManagedFunctionPtr;

        /*-----------------------------------------------------------------------------*/
        /* Helper Functions                                                            */
        /*-----------------------------------------------------------------------------*/
        /****************************************************************************//*!
        @brief    Retrieves a function pointer from HostFxr based on the specified
                  function name.

        @tparam        FunctionType
                Type of the function pointer that the specified function name will 
                provide.

        @params[in]    functionName
                Name of the HostFxr function to get a pointer to.

        @returns  Pointer to the function in the HostFxr that was specified.
        *//*****************************************************************************/
        template<typename FunctionType>
        FunctionType getHostFxrFunctionPtr(const std::string& functionName);
        template<typename FunctionType>
        FunctionType getCoreClrFunctionPtr(hostfxr_delegate_type functionType);
    };

    /*---------------------------------------------------------------------------------*/
    /* Template Function Implementations                                               */
    /*---------------------------------------------------------------------------------*/
    template<typename FunctionType>
    FunctionType DotNetRuntime::GetFunctionPtr(const string_t& assemblyName, 
                                               const string_t& typeName, 
                                               const string_t& functionName)
    {
        // Create the merged assembly and type string
        std::wstringstream wss;
        wss << typeName << L"," << assemblyName;

        FunctionType managedDelegate = nullptr;    
        int result = getManagedFunctionPtr
        (
            wss.str().c_str(),
            functionName.c_str(),
            UNMANAGEDCALLERSONLY_METHOD,
            NULL,
            NULL,
            (void**)&managedDelegate
        );

        // Check if it failed
        if (result < 0)
        {
            std::wstringstream wss;
            wss << std::hex << std::setfill(L'0') << std::setw(8) 
                << "[DotNetRuntime] Failed to get pointer to function \"" 
                << typeName << "." << functionName << "\" in assembly (" << assemblyName << "). "  
                << "Error 0x" << result << "\n";
            std::wstring wstr = wss.str();
            std::string str(wstr.length(), 0);
            std::transform(wstr.begin(), wstr.end(), str.begin(), [] (wchar_t c) 
            {
                return static_cast<char>(c);
            });
            throw FunctionPtrNotFoundException(str);
        }

        return managedDelegate;
    }
    template<typename FunctionType>
    FunctionType DotNetRuntime::getHostFxrFunctionPtr(const std::string& functionName)
    {
        FunctionType fPtr = reinterpret_cast<FunctionType>(GetProcAddress(hostFxrMod, functionName.c_str()));
        if (!fPtr)
        {
            std::ostringstream oss;
            oss << "[DotNetRuntime] Unable to get pointer to function: \"" << functionName << "\"";
            throw FunctionPtrNotFoundException(oss.str());
        }
        return fPtr;
    }
    template<typename FunctionType>
    FunctionType DotNetRuntime::getCoreClrFunctionPtr(hostfxr_delegate_type functionType)
    {
        void* fPtr = nullptr;

        // Get the load assembly function pointer
        int result = hostFxrGetDelegate
        (
            hostFxr,
            functionType,
            &fPtr
        );
        if (result != 0 || !fPtr)
        {
            std::ostringstream oss;
            oss << std::hex << std::showbase
                << "[DotNetRuntime] Error 0x" << result 
                << " Unable to get pointer to function of type : \"" << functionType << "\"";
            throw FunctionPtrNotFoundException(oss.str());
        }

        return reinterpret_cast<FunctionType>(fPtr);
    }
}

