#include <iostream>

#include "DotNetRuntime.h"

int wmain(int argc, const wchar_t** argv)
{
    // Start up CoreCLR via RAII
    Pls::DotNetRuntime dotnet(argc, argv);
    
    // Get function pointers to managed static functions (non-statics are not supported)
    using blankFunc = void(*)(void);
    blankFunc print = dotnet.GetFunctionPtr<blankFunc>
    (
        L"ManagedLibrary",
        L"ManagedLibrary.ManagedWorker",
        L"Print"
    );
    using powFunc = double(*)(double, double);
    powFunc power = dotnet.GetFunctionPtr<powFunc>
    (
        L"ManagedLibrary",
        L"ManagedLibrary.ManagedWorker",
        L"Power"
    );

    // Invoking managed functions
    print();
    std::cout << power(5.0, 3.0) << std::endl;

    return 0;
}